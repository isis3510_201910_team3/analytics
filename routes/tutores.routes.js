const express = require('express')
const router = express.Router()
const ach = require('../models/tutores.model')

router.get('/all', async (req, res) => {
    await ach.getTutoresFS()
    .then(achs => res.json(achs))
    .catch(err => {
        if (err.status) {
            res.status(err.status).json({ message: err.message })
        } else {
            res.status(500).json({ message: err.message})
        }
    })
})

router.get('/', async (req, res) => {
    await ach.getTutoresFS()
    .then(achs => res.json(achs))
    .catch(err => {
        if (err.status) {
            res.status(err.status).json({ message: err.message })
        } else {
            res.status(500).json({ message: err.message })
        }
    })
})

router.get('/edades', async (req, res) => {
    await ach.distribucionEdad()
    .then(achs => res.json(achs))
    .catch(err => {
        if (err.status) {
            res.status(err.status).json({ message: err.message })
        } else {
            res.status(500).json({ message: err.message })
        }
    })
})


router.get('/sexos', async (req, res) => {
    await ach.distribucionSexo()
    .then(achs => res.json(achs))
    .catch(err => {
        if (err.status) {
            res.status(err.status).json({ message: err.message })
        } else {
            res.status(500).json({ message: err.message })
        }
    })
})

router.get('/estratos', async (req, res) => {
    await ach.distribucionEstrato()
    .then(achs => res.json(achs))
    .catch(err => {
        if (err.status) {
            res.status(err.status).json({ message: err.message })
        } else {
            res.status(500).json({ message: err.message })
        }
    })
})

router.get('/numeroHijos', async (req, res) => {
    await ach.distribucionNumeroHijos()
    .then(achs => res.json(achs))
    .catch(err => {
        if (err.status) {
            res.status(err.status).json({ message: err.message })
        } else {
            res.status(500).json({ message: err.message })
        }
    })
})

router.get('/eps', async (req, res) => {
    await ach.distribucionEPS()
    .then(achs => res.json(achs))
    .catch(err => {
        if (err.status) {
            res.status(err.status).json({ message: err.message })
        } else {
            res.status(500).json({ message: err.message })
        }
    })
})

router.get('/edadPromedioTutores', async (req, res) => {
    await ach.edadPromedio()
    .then(achs => res.json(achs))
    .catch(err => {
        if (err.status) {
            res.status(err.status).json({ message: err.message })
        } else {
            res.status(500).json({ message: err.message })
        }
    })
})

router.get('/edadHombres', async (req, res) => {
    await ach.distribucionEdadHombres()
    .then(achs => res.json(achs))
    .catch(err => {
        if (err.status) {
            res.status(err.status).json({ message: err.message })
        } else {
            res.status(500).json({ message: err.message })
        }
    })
})

router.get('/edadMujeres', async (req, res) => {
    await ach.distribucionEdadMujeres()
    .then(achs => res.json(achs))
    .catch(err => {
        if (err.status) {
            res.status(err.status).json({ message: err.message })
        } else {
            res.status(500).json({ message: err.message })
        }
    })
})

router.get('/filtroSexoYNumeroHijos/sexo/:sexo/numeroHijos/:numeroHijos', async (req, res) => {
    let sexo = req.params.sexo;
    let numeroHijos = req.params.numeroHijos;

    await ach.filtroSexoYNumeroHijos(sexo, numeroHijos)
    .then(achs => res.json(achs))
    .catch(err => {
        if (err.status) {
            res.status(err.status).json({ message: err.message })
        } else {
            res.status(500).json({ message: err.message })
        }
    })
})



// Routes
module.exports = router