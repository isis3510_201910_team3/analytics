const fs = require('fs')
const axios = require("axios")

const filename = '../data/tutores.json'
let tutoresJSON = require(filename)
const filenameToWrite = './data/tutores.json'
tutoresJSON = []

let tutoresCFB = axios.get("https://firestore.googleapis.com/v1beta1/projects/bhelp-48327/databases/(default)/documents/tutores");

//promesa desde firestore
function getTutoresFS(){
    tutoresJSON = []
    writeJSONFile(filenameToWrite, tutoresJSON)
    return new Promise((resolve, reject) => {
        axios.all([tutoresCFB])
        .then(axios.spread(function (tutores) {

        let coleccionTutores = tutores.data;

        for(let i of coleccionTutores.documents){
            let tutorActual = i.fields
            tutoresJSON.push({
                "correo":tutorActual.correo.stringValue, 
                "eps":tutorActual.eps.stringValue, 
                "sexo":tutorActual.sexo.integerValue,
                "nombre":tutorActual.nombre.stringValue,
                "estrato":tutorActual.estrato.integerValue,
                "edad":tutorActual.edad.integerValue,
                "numeroHijos":tutorActual.numeroHijos.integerValue
            })
        }
        writeJSONFile(filenameToWrite, tutoresJSON)

        if (tutoresJSON.length === 0) {
            reject({
                message: 'No hay tutores registrados',
                status: 202
            })
        }

        resolve(tutoresJSON)

        })).catch((err) => {
        console.log(err);
        });
    })
}


//Obtener los tutores desde el JSON creado a partir de la promesa 
function getTutoresJSON(){
	return new Promise((resolve, reject) => {
        if (tutoresJSON.length === 0) {
            reject({
                message: 'No hay tutores registrados',
                status: 202
            })
        }
        resolve(tutoresJSON)
    })
}



function edadPromedio(){
    var count = 0;
    var obj = {};
    return new Promise((resolve, reject) => {
            tutoresJSON.forEach(function(el) {
              var key = el.id;
              obj[key] = obj[key] || { count: 0, total: 0, avg: 0 };
              obj[key].count++;
              obj[key].total += el.edad;
              obj[key].avg = obj[key].total / obj[key].count;
              resolve(obj[key].avg)
            });               
    })
}

function distribucionEdad(){
    return new Promise((resolve, reject) => {
        resolve(contarRepetidos(tutoresJSON,'edad'))
    })
}

function distribucionSexo(){
    return new Promise((resolve, reject) => {
        resolve(contarRepetidos(tutoresJSON,'sexo'))
    })
}

function distribucionEstrato(){
    return new Promise((resolve, reject) => {
        resolve(contarRepetidos(tutoresJSON,'estrato'))
    })
}

function distribucionNumeroHijos(){
    return new Promise((resolve, reject) => {
        resolve(contarRepetidos(tutoresJSON,'numeroHijos'))
    })
}

function distribucionEPS(){
    return new Promise((resolve, reject) => {
        resolve(contarRepetidos(tutoresJSON,'eps'))
    })
}

function contarRepetidos(datos, propiedad) {
    return datos
      .reduce((res, item) => Object
        .assign(res, {
          [item[propiedad]]: 1 + (res[item[propiedad]] || 0)
        }), Object.create(null));
  }

function writeJSONFile(filename, content) {
    fs.writeFile(filename, JSON.stringify(content), 'utf8', (err) => {
        if (err) {
            console.log(err)
        }
    })
}

function distribucionEdadHombres(){
    return new Promise((resolve, reject) => {
        //console.log("SS",filtrarTutotes( tutoresJSON,{sexo:0} ))
        resolve(contarRepetidos(filtrarTutotes( tutoresJSON,{sexo:0}),'edad'))
    })
}

function distribucionEdadMujeres(){
    return new Promise((resolve, reject) => {
        console.log("SS",filtrarTutotes( tutoresJSON,{sexo:1} ))
        resolve(contarRepetidos(filtrarTutotes( tutoresJSON,{sexo:1}),'edad'))
    })
}

function filtroSexoYNumeroHijos(pSexo, pNumeroHijos){
    return new Promise((resolve, reject) => {
        if (filtrarTutotes( tutoresJSON,{sexo:pSexo, numeroHijos:pNumeroHijos})===undefined) {
            reject({
                message: 'No hay tutores que cumplan los parametros ingresados',
                status: 202
            })
        }
        resolve(filtrarTutotes( tutoresJSON,{sexo:pSexo, numeroHijos:pNumeroHijos}))
    })
}

function filtrarTutotes(datos, propiedad){

    return datos.filter(function(obj) {
      return Object.keys(propiedad).every(function(c) {
        return obj[c] == propiedad[c];
      });
    });
  
  }

module.exports = {
    getTutoresFS,
    getTutoresJSON,
    distribucionEdad,
    distribucionSexo,
    distribucionEstrato,
    distribucionNumeroHijos,
    distribucionEPS,
    edadPromedio,
    distribucionNumeroHijos,
    edadPromedio,
    distribucionEdadHombres,
    distribucionEdadMujeres,
    filtroSexoYNumeroHijos
}